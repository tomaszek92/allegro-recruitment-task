# Allegro recruitment task

## Unit tests
Run command `dotnet test` in `GitHubRepositoryStatistics.UnitTests` folder.

## End-to-end test

Run command `dotnet test` in `GitHubRepositoryStatistics.E2eTests` folder.

## Deployment to production
Run command `dotnet build -c Release` and `dotnet run -c Release` in `GithubRepositoryStatistics` folder. The application will work on https://localhost:5001.